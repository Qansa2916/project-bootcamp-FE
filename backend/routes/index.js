import Express from "express";
import { getusers,Register,Login,Logout, getuserbyid, getuserbyname, UpdateUser, DeleteUser, AddUser, UpdatePassword } from "../controllers/Users.js";
import { verifyToken } from "../Middleware/VerifyToken.js";
import { refreshToken } from "../controllers/RefreshToken.js";
import { AddProduk, DeleteProduk, getProduk, getProdukbyid, getProdukbyname, UpdateProduk } from "../controllers/Produk.js";
import { AddRoles, DeleteRoles, getRoles, getRolesbyid, getRolesbyname, UpdateRoles } from "../controllers/Roles.js";
import { AddCustomer, DeleteCustomer, getCustomer, getCustomerbyid, getCustomerbyname, UpdateCustomer } from "../controllers/Customer.js";
import { AddTransaksi, DeleteTransaksi, getTransaksi, getTransaksibyid, getTransaksibyname, UpdateTransaksi } from "../controllers/Transaksi.js";
import { DeleteTransaksiDetail, getTransaksiDetail, getTransaksiDetailbyid, UpdateTransaksiDetail } from "../controllers/TransaksiDetail.js";
const router= Express.Router();

router.get('/users',verifyToken,getusers);
router.get('/users/:id',verifyToken,getuserbyid);
router.get('/userbyname',verifyToken,getuserbyname);
router.post('/users',Register);
router.post('/adduser',verifyToken,AddUser);
router.put('/users/:id',verifyToken,UpdateUser);
router.put('/resetpassword/:id',verifyToken,UpdatePassword);
router.delete('/users/:id',verifyToken,DeleteUser);
router.post('/login',Login);
router.get('/token',refreshToken);
router.delete('/logout',Logout);

//route produk
router.get('/produk',verifyToken,getProduk);
router.get('/produk/:id',verifyToken,getProdukbyid);
router.get('/produkbyname',verifyToken,getProdukbyname);
router.post('/produk',verifyToken,AddProduk);
router.put('/produk/:id',verifyToken,UpdateProduk);
router.delete('/produk/:id',verifyToken,DeleteProduk);

//route roles
router.get('/roles',getRoles);
router.get('/roles/:id',getRolesbyid);
router.get('/rolesbyname',getRolesbyname);
router.post('/roles',verifyToken,AddRoles);
router.put('/roles/:id',verifyToken,UpdateRoles);
router.delete('/roles/:id',verifyToken,DeleteRoles);

//router customer
router.get('/customers',getCustomer);
router.get('/customers/:id',getCustomerbyid);
router.get('/customersbyname',getCustomerbyname);
router.post('/customers',verifyToken,AddCustomer);
router.put('/customers/:id',verifyToken,UpdateCustomer);
router.delete('/customers/:id',verifyToken,DeleteCustomer);

// router transaksi
router.get('/transaksi',getTransaksi);
router.get('/transaksi/:id',getTransaksibyid);
router.get('/transaksibyname',getTransaksibyname);
router.post('/transaksi',verifyToken,AddTransaksi);
router.put('/transaksi/:id',verifyToken,UpdateTransaksi);
router.delete('/transaksi/:id',verifyToken,DeleteTransaksi);

// router transaksi detail
router.get('/transaksidetail',getTransaksiDetail);
router.get('/transaksidetail/:id',getTransaksiDetailbyid);
router.put('/transaksidetil/:id',verifyToken,UpdateTransaksiDetail);
router.delete('/transaksidetail/:id',verifyToken,DeleteTransaksiDetail);

export default router;