import { DataTypes,Sequelize } from "sequelize";
import db from "../config/Database.js";

const Transaksi=db.define('transaksi',{
    nama_customer:{
        type: DataTypes.STRING
    },
    produk:{
        type: DataTypes.STRING
    },
    harga:{
        type: DataTypes.FLOAT
    },
    tanggal:{
        type: DataTypes.DATE
    },
    quantity:{
        type: DataTypes.FLOAT
    }
},{
    freezeTableName:true
});

export default Transaksi;