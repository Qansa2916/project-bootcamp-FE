import { DataTypes,Sequelize } from "sequelize";
import db from "../config/Database.js";

const TransaksiDetail=db.define('transaksi_detail',{
    transaksi_id:{
        type: DataTypes.STRING
    },
    produk_id:{
        type: DataTypes.STRING
    },
    harga_produk:{
        type: DataTypes.FLOAT
    },
    quantity:{
        type: DataTypes.INTEGER
    }
},{
    freezeTableName:true
});

export default TransaksiDetail;