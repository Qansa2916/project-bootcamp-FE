import {Op} from 'sequelize';
import TransaksiDetail from '../models/TransaksiDetailModel.js';

export const getTransaksiDetail = async (req, res) => {
    try {
        const Transaksidetails = await TransaksiDetail.findAll({
            attributes: ['id', 'transaksi_id', 'produk_id', 'harga_produk', 'quantity']
        });
        res.json(Transaksidetails);
    } catch (error) {
        console.log(error);
    }
}

export const getTransaksiDetailbyid = async (req, res) => {
    const id = req.params.id;
    try {
        const Transaksidetails = await TransaksiDetail.findByPk(id);
        res.json(Transaksidetails);
    } catch (error) {
        console.log(error);
    }
}

// export const getTransaksiDetailbyname = async (req, res) => {
//     const nama_customer = req.body.name;
//     var condition = nama_customer ? { nama_customer: { [Op.like]: `%${nama_customer}%` } } : null;
//     try {
//         const Transaksis = await Transaksi.findAll({ where: condition });
//         res.json(Transaksis);
//     } catch (error) {
//         console.log(error);
//     }
// }

// export const AddTransaksi = async (req, res) => {
//     const { transaksi_id, produk_id, harga_produk,quantity } = req.body;
//     try {
//         await TransaksiDetail.create({
//             transaksi_id: transaksi_id,
//             produk_id : produk_id,
//             harga_produk : harga_produk,
//             quantity : quantity
//         });
//         res.json({ msg: "Tambah Transaksi Detail Berhasil" });
//     } catch (error) {
//         console.log(error);
//     }
// }

export const UpdateTransaksiDetail = async (req, res) => {
    const id = req.params.id;
    const { transaksi_id, produk_id, harga_produk,quantity } = req.body;
    const data = {
        transaksi_id : transaksi_id,
        produk_id : produk_id,
        harga_produk : harga_produk,
        quantity : quantity
    }
    try {
        await TransaksiDetail.update(
            data, {
            where: {
                id: id
            }
        }
        );
        res.json({ msg: "Update Transaksi Detail Berhasil" });
    } catch (error) {
        console.log(error);
    }
}

export const DeleteTransaksiDetail = async (req, res) => {
    const id = req.params.id;
    try {
        await TransaksiDetail.destroy({
            where: {
                id: id
            }
        });
        res.json({ msg: "Berhasil Hapus Transaksi Detail" });
    } catch (error) {
        console.log(error);
    }
}

