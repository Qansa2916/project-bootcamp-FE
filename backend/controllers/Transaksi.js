import {Op} from 'sequelize';
import Transaksi from '../models/TransaksiModel.js';

export const getTransaksi = async (req, res) => {
    try {
        const Transaksis = await Transaksi.findAll({
            attributes: ['id', 'nama_customer', 'produk', 'harga', 'tanggal', 'quantity']
        });
        res.json(Transaksis);
    } catch (error) {
        console.log(error);
    }
}

export const getTransaksibyid = async (req, res) => {
    const id = req.params.id;
    try {
        const Transaksis = await Transaksi.findByPk(id);
        res.json(Transaksis);
    } catch (error) {
        console.log(error);
    }
}

export const getTransaksibyname = async (req, res) => {
    const nama_customer = req.body.name;
    var condition = nama_customer ? { nama_customer: { [Op.like]: `%${nama_customer}%` } } : null;
    try {
        const Transaksis = await Transaksi.findAll({ where: condition });
        res.json(Transaksis);
    } catch (error) {
        console.log(error);
    }
}

export const AddTransaksi = async (req, res) => {
    const { nama_customer, produk, harga, tanggal, quantity } = req.body;
    try {
        await Transaksi.create({
            nama_customer: nama_customer,
            produk : produk,
            harga: harga,
            tanggal : tanggal,
            quantity : quantity
        });
        res.json({ msg: "Tambah Transaksi Berhasil" });
    } catch (error) {
        console.log(error);
    }
}

export const UpdateTransaksi = async (req, res) => {
    const id = req.params.id;
    const { nama_customer, produk, harga, tanggal, quantity} = req.body;
    const data = {
        nama_customer: nama_customer,
        produk : produk,
        harga: harga,
        tanggal : tanggal,
        quantity : quantity
    }
    try {
        await Transaksi.update(
            data, {
            where: {
                id: id
            }
        }
        );
        res.json({ msg: "Update Transaksi Berhasil" });
    } catch (error) {
        console.log(error);
    }
}

export const DeleteTransaksi = async (req, res) => {
    const id = req.params.id;
    try {
        await Transaksi.destroy({
            where: {
                id: id
            }
        });
        res.json({ msg: "Berhasil Hapus Transaksi" });
    } catch (error) {
        console.log(error);
    }
}

