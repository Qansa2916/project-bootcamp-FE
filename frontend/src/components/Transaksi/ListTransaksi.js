import React, { useEffect, useState } from 'react';
import axios from 'axios';
import jwt_decode from 'jwt-decode';
import { useNavigate } from 'react-router-dom';
import swal from 'sweetalert';
import DataTable from 'react-data-table-component';

const ListTransaksi = () => {
//   const [name, setName] = useState('');
  const [role, setRole] = useState('');
  const [token, setToken] = useState('');
  const [expired, setExpired] = useState('');
  const [transaksi, setTransaksi] = useState([]);
  const history = useNavigate();

  const axiosJwt = axios.create();

  useEffect(() => {
    refreshToken();
    getTransaksi();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const refreshToken = async () => {
    try {
      const response = await axios.get('http://localhost:5000/token');
      setToken(response.data.accessToken);
      const decode = jwt_decode(response.data.accessToken);
      // setName(decode.name);
      setRole(decode.role);
      setExpired(decode.exp);
    } catch (error) {
      if (error.response) {
        history('/');
      }
    }
  }

  axiosJwt.interceptors.request.use(async (config) => {
    const currentDate = new Date();
    if (expired * 1000 < currentDate.getTime()) {
      const response = await axios.get('http://localhost:5000/token');
      config.headers.Authorization = `Bearer ${response.data.accessToken}`;
      setToken(response.data.accessToken);
      const decode = jwt_decode(response.data.accessToken);
      // setName(decode.name);
      setRole(decode.role);
      setExpired(decode.exp);
    }
    return config;
  }, (error) => {
    return Promise.reject(error);
  }
  );

  const getTransaksi = async () => {
    const response = await axiosJwt.get('http://localhost:5000/transaksi', {
      headers: {
        Authorization: `Bearer ${token}`
      }
    });
    setTransaksi(response.data);
  }

  function addTransaksi() {
    history('/add-Transaksi/_add');
  }

  function editTransaksi(id) {
    history(`/add-Transaksi/${id}`);
  }

  function viewTransaksi(id) {
    history(`/view-Transaksi/${id}`);
  }

  const deleteTransaksi = async (id) => {


    var proceed = window.confirm('Apakah anda yakin hapus?');
    if (proceed) {
      const response = await axiosJwt.delete('http://localhost:5000/transaksi/' + id, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
      swal(response.data.msg);
      const NewCustomers = transaksi.filter(Transaksi => Transaksi.id !== id);
      setTransaksi(NewCustomers);
    } else {
      // swal('batal hapus');
    }
  }

  const columns = [
    {
      name: 'Id',
      width: '50px',
      cell: (row) => {
        return <div>{row.id}</div>;
      },
      shortable: true
    },
    {
      name: 'Nama Customer',
      selector: row => row.nama_customer,
      width: '200px',
      sortable: true
    },
    {
      name: 'Produk',
      selector: row => row.produk,
      width: '200px',
      sortable: true
    },
    {
        name: 'Harga',
        selector: row => row.harga,
        width: '200px',
        sortable: true
      },       
      {
        name: 'Tanggal',
        selector: row => row.tanggal,
        width: '200px',
        sortable: true
      },    
      {
        name: 'Quantity',
        selector: row => row.quantity,
        width: '200px',
        sortable: true
      },
      {
        name: 'Total Harga',
        selector: row => (row.harga*row.quantity),
        width: '200px',
        sortable: true
      }, 
    {
      name: 'Action',
      width: '400px',
      cell: (row) =>
        <div>
          <button onClick={() => editTransaksi(row.id)}
            className='button is-default'>Edit</button>
          <button style={{ marginleft: "10px" }}
            onClick={() => deleteTransaksi(row.id)}
            className='button is-danger'>Delete</button>
          <button style={{ marginleft: "10px" }}
            onClick={() => viewTransaksi(row.id)}
            className='button is-success'>View</button>

        </div>
      ,
      ignoreRowClick: true,
      allowOverflow: true,
      button: true,
    },
  ];

  const data = transaksi;

  // const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;

  function MyComponent() {
    if (role !== 'admin') {
      return (
        <div>
          <h3>Data Produk</h3>
          <button onClick={addTransaksi} className='button is-info'>Add Transaksi</button>
          <DataTable
            columns={columns}
            data={data}
            pagination
          />
        </div>
      );
  } else {
    return(
      <div>
        <h3>Aplikasi React Js</h3>
      </div>
    )
  }
}

  return (
    <div className='container mt-5'>
      <h1>Transaksi</h1>
      <hr></hr>
      {MyComponent()}


    </div>
  )
}

export default ListTransaksi
