import React, { useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom';
import axios from 'axios';
import jwt_decode from 'jwt-decode';
import swal from 'sweetalert';

const CreateTransaksi = () => {
  let params = useParams();
  const [id, setId] = useState(params.id);
  const [nama_customer, setNama_Customer] = useState('');
  const [produk, setProduk] = useState('');
  const [harga, setHarga] = useState('');
//   const [tanggal, setTanggal] = useState('');
  const [quantity, setQuantity] = useState('');
  const [token, setToken] = useState('');
  const [produks, setProduks] = useState([]);
  const [nama_customers, setNama_Customers] = useState([]);
  const [expired, setExpired] = useState('');
  
  const history = useNavigate();
  const axiosJwt = axios.create();

  const refreshToken = async () => {
    try {
      const response = await axios.get('http://localhost:5000/token');
      setToken(response.data.accessToken);
      const decode = jwt_decode(response.data.accessToken);
    //   setName(decode.name);
      setExpired(decode.exp);
    } catch (error) {
      if (error.response) {
        history('/');
      }
    }
  }

  axiosJwt.interceptors.request.use(async (config) => {
    const currentDate = new Date();
    if (expired * 1000 < currentDate.getTime()) {
      const response = await axios.get('http://localhost:5000/token');
      config.headers.Authorization = `Bearer ${response.data.accessToken}`;
      setToken(response.data.accessToken);
      const decode = jwt_decode(response.data.accessToken);
      // setName(decode.name);
      setExpired(decode.exp);
    }
    return config;
  }, (error) => {
    return Promise.reject(error);
  }
  );

  useEffect(() => {
    refreshToken();
    getCustomer();
    getProduk();
    cekId();
    // eslint-disable-next-line
  }, []);

  const cekId = async () => {
    if (id === '_add') {
      setNama_Customer('');
      setProduk('');
      setHarga('');
    //   setTanggal('');
      setQuantity('');
      return
    } else {
      const res = await axiosJwt.get('http://localhost:5000/transaksi/' + id, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
      let Transaksi = res.data;
      setId(Transaksi.id);
      setNama_Customer(Transaksi.nama_customer);
      setProduk(Transaksi.produk);
      setHarga(Transaksi.harga);
    //   setTanggal(Transaksi.tanggal);
      setQuantity(Transaksi.quantity);
    }
  }

  
  const getProduk = async () => {
    const response = await axiosJwt.get('http://localhost:5000/produk');
    setProduks(response.data);
    setProduk(response.data[0].nama)
}

const getCustomer = async () => {
    const response = await axiosJwt.get('http://localhost:5000/customers');
    setNama_Customers(response.data);
    setNama_Customer(response.data[0].name)
}

  const saveOrUpdateTransaksi = async (e) => {
    e.preventDefault();
    let Transaksi = {
      nama_customer : nama_customer,
      produk : produk,
      harga : harga,
    //   tanggal : tanggal,
      quantity : quantity,
    }

    if (id === '_add') {
      await axios.post('http://localhost:5000/transaksi', Transaksi,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })
        .then(res => {
          swal(res.data.msg);
          history('/transaksi');
        });
    } else {
      await axios.put('http://localhost:5000/transaksi/' + id, Transaksi,
        {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })
        .then(res => {
          swal(res.data.msg);
          history('/transaksi');
        });

    }
  }

  function cancel() {
    history('/transaksi');
  }

  function getTitle() {
    if (id === "_add") {
      return <h3 className="text-center">Add Transaksi</h3>
    } else {
      return <h3 className="text-center">Update Transaksi</h3>
    }
  }

  const InputHargaProduk = () => {
    produks.map(HargaProduk)
  }

  function HargaProduk (item) {
    if (produk === item.nama) {
      setHarga(item.harga)
    }
  }

  function DropdownProduk() { 
      return (
          <div className='form-group'>
              <label>Nama Produk</label>
              <div className='controls'>
                  <select className='form-control' name='nama' id='nama' onChange={(e) => setProduk(e.target.value)} onClick={() => InputHargaProduk()} value={produk}>
                      {produks.map((produk) => (
                          <option key={produk.id}>{produk.nama}</option>
                      ))}
                  </select>
              </div>
          </div>
      )
  }

  function DropdownCustomer() {
    return (
        <div className='form-group'>
            <label>Nama Customer</label>
            <div className='controls'>
                <select className='form-control' name='nama_customer' id='nama' onChange={(e) => setNama_Customer(e.target.value)} value={nama_customer}>
                    {nama_customers.map((customer) => (
                        <option key={customer.id}>{customer.name}</option>
                    ))}
                </select>
            </div>
        </div>
    )
}

  return (
    <div>
      <br></br>
      <div className="container">
        <div className="row">
          <div className="card col-md-6 offset-md-3 offset-md-3">
            {getTitle()}
          </div>
          <div className="card-body">
            <form>
              {DropdownCustomer()}
              {DropdownProduk()}
              <div className="form-group">
                <label>Quantity</label>
                <input placeholder="Quantity" name="quantity" type="int" className="form-control"
                  value={quantity} onChange={(e) => setQuantity(e.target.value)} />
              </div>
              <br></br>
              <button className="btn btn-success" onClick={saveOrUpdateTransaksi} >Simpan</button>
              <button className="btn btn-danger" onClick={cancel}>Batal</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CreateTransaksi
